# -----------------------------------------------------------------------------
# Para probar la parte opcional CRUD DE USUARIOS necesitarás contenedores
# -----------------------------------------------------------------------------
Prerequisitos: Tu Sistema Operativo debe tener disponibles los puertos 11001 y 11002.
1.- Instala "Docker Desktop".
2.- Abre una consola de línea de comandos en tu sistema operativo.
3.- Dirígete a la carpeta de este repositorio, es decir "parrot-challenge-jaimezaballa"
4.- Executa el comando "docker-compose --file parrot-challenge-jaimezaballa.yml --project-name parrot-challenge-jaimezaballa up --detach".
5.- Espera unos segundos en lo que los contenedores se terminan de cargar.
6.- Executa el comando "docker exec -i parrotapi php artisan migrate:refresh --seed".
7.- Disfruta :)
# -----------------------------------------------------------------------------
# Posibles errores
# -----------------------------------------------------------------------------
ERROR: Si al ejecutar el paso 6 te aparecen errores de Laravel y MySQL como "Connection refused".
SOLUCIÓN: Solo espera unos segundos y vuelve a intentarlo, ya que los contenedores de docker tardan unos momentos en estar totalmente disponibles.
# -----------------------------------------------------------------------------
# Pruebas unitarias
# -----------------------------------------------------------------------------
a) docker exec -i parrotapi php artisan test
En las pruebas se realizan las comprobaciones
[Tests\Feature\AuthTest]
* register
* authenticate with token works
* register duplicate should not pass
* register without parameters should not pass
[Tests\Feature\OrdersTest]
* getting token
* authenticate with token works
* order create ideal works
* order create bad estructure
# -----------------------------------------------------------------------------
# POSTMAN
# -----------------------------------------------------------------------------
Se puede utilizar POSTMAN y cargar el archivo parrot.postman_collection.json
Posteriormente se debe crear el entorno parrot en POSTMAN y crear siguiente variables
[Postman-Variables]
token=1|dL1d0rO4LZ5qpfjLZydkgaOGQoTGcvDHmVNVbqnP
xsrf-cookie=null
url=http://localhost:11002
[Postman-Collection]
Register
Creare Order
Report-Selled-Products by Datetime Hour
Report-Selled-Products by Datetime Hour and Minute
Report-Selled-Products by Datetime Range
User Information
SPA Get-CSRF-Token
Login
Logout