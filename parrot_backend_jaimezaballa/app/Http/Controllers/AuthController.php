<?php

namespace App\Http\Controllers {

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Validator;

    use App\Http\Controllers\Controller;
    use App\Models\User;
    use Illuminate\Support\Facades\Auth;

class AuthController extends Controller {
        
        public function register(Request $request) {

            $validator = Validator::make($request->all(), [
                'name'=>'required|string',
                'email'=>'required|string|unique:users,email',
                'password'=>'required|string|confirmed'
            ],[
                'name.required'=>'El nombre de usuario es obligatorio',
                'email.required'=>'El correo electrónico es obligatorio',
                'email.unique'=>'El correo :input ya esta ocupado',
                'password.required'=>'Debes especificar una contraseña',
                'password.confirmed'=>'La confirmación de la contraseña no coincide'
            ]);

            if ($validator->fails()) {

                $messages = collect($validator->errors())->map(function($messages, $field){
                    return (is_array($messages) ? $messages[0] : $messages);
                });
                $response = [
                    "success"=>false,
                    "messages"=>$messages
                ];
                return response()->json($response, 400);

            } else {

                $fields = $validator->validated();
                $user = User::create([
                    'name'=>$fields['name'],
                    'email'=>$fields['email'],
                    'password'=>bcrypt($fields['password'])
                ]);
    
                $token = $user->createToken('myapptoken')->plainTextToken;
                $response = [
                    'success'=>true, 
                    'user'=> $user, 
                    'token'=>$token
                ];
    
                return response()->json($response, 201);
            }
        }

        public function logout(Request $request) {
            $response = ["success"=>true, "messages"=>["Se cerró la sessión de forma satisfactoria"]];
            Auth::logout();
            $request->session()->invalidate();
            $request->session()->regenerateToken();
            return response()->json($response, 201);
        }

        // It's called from web routes
        public function login(Request $request) {
            $response = ["success"=>false];
            $credentials = $request->validate([
                'email' => 'required|email',
                'password' => 'required',
            ], [
                'email.required' => 'El correo electrónico es requerido',
                'email.email' => 'Debes proporcionar un correo válido',
                'password.required' => 'Por favor escribe tu contraseña'
            ]);
            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();
                $response["success"] = true;
            }
            return response()->json($response, 201);
        }

    }

}

?>