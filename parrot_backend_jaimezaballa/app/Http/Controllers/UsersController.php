<?php

namespace App\Http\Controllers {

    use App\Models\User;
    use Illuminate\Http\Request;

    class UsersController extends Controller {
        
        public function index(Request $request) {
            return response()->json(["user"=>$request->user()], 201);
        }

    }


}

?>