<?php

namespace App\Http\Controllers {

    use App\Http\Structures\FiltersDatesStructure;
    use App\Models\OrdersProducts;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;

    class ReporteProductosVendidos extends Controller {
        
        public function index(Request $request) {

            $response = ["success"=>false];

            if (!$request->has("date")) {
                
                $response["messages"][] = "Debes especificar por lo menos una fecha o rango de fechas para generar el reporte";

            } else {

                $date = $request->input("date");
                $validDateMessages = FiltersDatesStructure::validate($date);
                if (!empty($validDateMessages)) {

                    $response["messages"] = $validDateMessages;

                } else {

                    $headers = [
                        "name"=>"Nombre del producto", 
                        "quantity"=>"Cantidad Total",
                        "total"=> "Precio Total"
                    ];
                    
                    // Agrupamos por cantidad de productos y precio de venta
                    $model = OrdersProducts::select("products.name", DB::raw('SUM(orders_products.quantity) as quantity'), DB::raw('SUM(orders_products.quantity * orders_products.unitprice) as total'));
                    
                    // Agregamos una relación con la tabla de productos para extraemos el nombre del producto
                    $model->join("products", "products.id", "orders_products.product_id");

                    // Si es un arreglo y ese arreglo tiene exáctamente 2 elementos, 
                    // Se trata de un rango de fechas (inicio y fin)
                    $filterDate = $date;
                    if (is_array($date)) {
                        $model->whereBetween("orders_products.created_at", $filterDate);
                    } else {
                        $dp = date_parse($date);
                        // Si el usuario no ha especificado ningún dato de horario
                        $strdate = $dp["year"]."-".$dp["month"]."-".$dp["day"];
                        $hour = sprintf("%02d", $dp["hour"]);
                        $minute = sprintf("%02d", $dp["minute"]);
                        $second = sprintf("%02d", $dp["second"]);
                        if (empty($dp["hour"]) && empty($dp["minute"]) && empty($dp["second"])) {
                            $from = $strdate." 00:00:00";
                            $to = $strdate." 23:59:59";
                            $filterDate = [$from, $to];
                            $model->whereBetween("orders_products.created_at", $filterDate);
                        } else if (!empty($dp["hour"]) && empty($dp["minute"]) && empty($dp["second"])) {
                            $from = $strdate." ".$hour.":00:00";
                            $to = $strdate." ".$hour.":59:59";
                            $filterDate = [$from, $to];
                            $model->whereBetween("orders_products.created_at", $filterDate);
                        } else if (!empty($dp["hour"]) && !empty($dp["minute"]) && empty($dp["second"])) {
                            $from = $strdate." ".$hour.":".$minute.":00";
                            $to = $strdate." ".$hour.":".$minute.":59";
                            $filterDate = [$from, $to];
                            $model->whereBetween("orders_products.created_at", $filterDate);
                        } else {
                            $model->where("orders_products.created_at", $filterDate);
                        }
                    }

                    // Informamos al usuario que fechas se utilizaron para realizar el filtro
                    $response["date"] = $filterDate;

                    // Agrupamos por ID de producto para que las sumatorias sean correctas
                    // Entonces ordenamos por cantidad que indica el más vendido (NO necesariamente el más redituable)
                    $products = $model->groupBy("product_id")->orderBy("quantity", "DESC")->get();

                    if ($products->count() == 0) {
                        $response["messages"][] = "No se encontraron productos vendidos para las fechas seleccionadas";
                    } else {
                        $response["success"] = true;
                        $response["data"] = $products;
                    }
                    
                }
            }

            return response()->json($response, 200);
        }
        
    }

}

?>