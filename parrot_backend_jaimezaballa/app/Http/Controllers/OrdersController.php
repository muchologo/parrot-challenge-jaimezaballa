<?php

namespace App\Http\Controllers {

    use App\Http\Structures\ProductsStructure;
    use App\Http\Utils\TextUtils;
    use App\Models\Logs;
    use App\Models\Orders;
    use App\Models\OrdersProducts;
    use App\Models\Products;
    use App\Models\User;

    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Http\Request;
    use Normalizer;

class OrdersController extends Controller {
        
        public function store(Request $request) {
            
            $response = ["success"=>false];
            $httpCode = 400;

            // Validamos que la orden tenga al menos un producto
            if (!$request->has("products")) {
                $response["messages"][] = "La orden debe contener al menos un producto.";
                return response()->json($response, $httpCode);
            }

            // Validamos la estructura de la petición
            $products = $request->input("products");
            $structureMessages = ProductsStructure::validate($products);
            if (!empty($structureMessages)) {
                $response["messages"] = $structureMessages;
                return response()->json($response, $httpCode);
            }

            DB::beginTransaction();
            try {

                // Validamos los datos de la orden y luego los datos de cada producto
                $orderData = $request->only(["client"]);
                // El ID de usuario lo tamomos del usuario que realizó la petición
                $orderData["user_id"] = $request->user()->id;
                // Más tarde calcularémos el total al dar de alta los productos
                $orderData["total"] = 0;

                $validator = Validator::make($orderData, Orders::$createDefinition, Orders::$createMessages);
                if ($validator->fails()) {

                    $validatorMessages = collect($validator->errors())->map(function($messages, $field){
                        return (is_array($messages) ? $messages[0] : $messages);
                    })->toArray();
                    $response["messages"]["order"] = $validatorMessages;

                } else {

                    // Creamos la orden
                    $order = Orders::create($orderData);
                    $orderData = $order->toArray();

                    // Validamos los datos de cada producto
                    $productosRelacionados = 0;
                    $precioTotal = 0;
                    foreach ($products as $index=>$productData) {

                        $productData["order_id"] = $order->id;
                        $productData["slug"] = isset($productData["name"]) ? TextUtils::slug($productData["name"]) : null;

                        $validator = Validator::make($productData, Products::$createDefinition, Products::$createMessages);

                        // Avisamos al usuario de errores encontrados con los productos
                        $orderProductData = false;
                        if ($validator->fails()) {
                            $validatorMessages = collect($validator->errors())->map(function($messages, $field){
                                return (is_array($messages) ? $messages[0] : $messages);
                            })->toArray();

                            // Si solo hubo un error y ese error se refiere a que el producto ya existe
                            // Seguímos un procedimiento para extraer información sobre el producto y agregarlo a la orden
                            if (count($validatorMessages) == 1 && isset($validatorMessages["name"])) {
                                if ($validatorMessages["name"] == "Ese nombre de producto ya esta ocupado") {

                                    // Obtenemos el producto desde la BD para crear la relación
                                    // Pero ya no creamos el producto
                                    $productBD = Products::where("slug", $productData["slug"])->first();
                                    $orderProductData = [
                                        "order_id"=>$order->id,
                                        "product_id"=>$productBD->id,
                                        "name"=>$productData["name"],
                                        "unitprice"=>$productData["unitprice"],
                                        "quantity"=>$productData["quantity"]
                                    ];
                                }
                            // En caso contrario enviamos los errrores encontrados al usuario
                            } else {
                                $response["messages"]["producto.".$index] = $validatorMessages;
                            }
                        
                        } else {

                            // Cuando no se detectan fallos en los datos del producto
                            // Se trata de un producto nuevo, así que lo creamos
                            $newProductData = [
                                "slug"=>$productData["slug"],
                                "name"=>$productData["name"],
                                "unitprice"=>$productData["unitprice"],
                            ];
                            if (isset($productData["description"])) {
                                $newProductData["description"] = $productData["description"];
                            }
                            $new = Products::create($newProductData);
                            $orderProductData = [
                                "order_id"=>$order->id,
                                "product_id"=>$new->id,
                                "name"=>$productData["name"],
                                "unitprice"=>$productData["unitprice"],
                                "quantity"=>$productData["quantity"]
                            ];
                        }

                        // Si hay datos sobre el producto a relacionar con la orden
                        if (!empty($orderProductData)) {
                            $orderProduct = OrdersProducts::create($orderProductData);
                            $subtotal = ($orderProduct->unitprice * $orderProduct->quantity);
                            $precioTotal += $subtotal;
                            $orderData["products"][] = $orderProduct->toArray();
                            $productosRelacionados++;
                        }
                    }
                    
                    // Para no crear ordenes vacias
                    // Solo aplicamos la transacción cuando hay 1 o más productos relacionados a la orden
                    if ($productosRelacionados > 0) {
                        $order->total = $precioTotal;
                        $order->save();
                        $orderData["total"] = $precioTotal;
                        $response['success'] = true;
                        $response["order"] = $orderData;
                        $httpCode = 201;
                        DB::commit();
                    } else {
                        DB::rollback();
                    }
                }

            // Cualquier error imprevisto lo enviarémos al log de la base de datos
            // Al usuario le enviarémos un error 500 y un mensaje más amigable
            } catch (\Exception $e) {

                DB::rollback();
                Logs::create([
                    "user_id"=>$request->user()->id,
                    "controller"=>self::class,
                    "action"=>__FUNCTION__,
                    "message"=>$e->getMessage()
                ]);
                $response['messages'][] = "Tu solicitud no pudo ser procesada";
                $httpCode = 500;
            }

            return response()->json($response, $httpCode);
        }
        
    }


}

?>