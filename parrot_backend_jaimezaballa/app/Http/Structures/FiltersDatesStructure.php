<?php

namespace App\Http\Structures {

    class FiltersDatesStructure {

        protected static $shortFormat = 'Y-m-d';
        protected static $longFormat = 'Y-m-d H:i:s';

        private static function validateDate($date) {
            $shortDate = \DateTime::createFromFormat(self::$shortFormat, $date);
            $shortValid = $shortDate && $shortDate->format(self::$shortFormat) == $date;

            $longDate = \DateTime::createFromFormat(self::$longFormat, $date);
            $longValid = $longDate && $longDate->format(self::$longFormat) == $date;
            return $shortValid || $longValid;
        }
        
        public static function validate($date):array {
            $messages = [];
            if (is_array($date)) {
                if (count($date) !=2 ) {
                    $messages[] = "Si envias un rango de fechas asegurate de especificar exáctamente dos.";
                }
                $validDates = collect($date)->reduce(function($areValid, $d){
                    return $areValid && self::validateDate($d);
                }, true);
            } else {
                $validDates = self::validateDate($date);
            }
            if (!$validDates) { $messages[] = "Las fechas deben tener formato corto (2021-10-13) o formato largo (2021-10-13 20:44:18)"; }
            return $messages;
        }

    }

}

?>