<?php

namespace App\Http\Structures {

    class ProductsStructure {

        private static $validKeys = [
            "name",
            "description",
            "unitprice",
            "quantity"
        ];

        public static function validate(array $products):array {
            $messages = [];
            if (is_array($products)) {
                foreach ($products as $index=>$product) {
                    foreach ($product as $key=>$value) {
                        $validKey = in_array($key, self::$validKeys);
                        if (!$validKey) { $messages["producto.".$index][] = "No se reconoce la clave ".$key; }
                    }
                }
            }
            return $messages;
        }

    }

}

?>