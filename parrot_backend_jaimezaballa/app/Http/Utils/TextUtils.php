<?php

namespace App\Http\Utils {

    use Normalizer;

    class TextUtils {

        private static $combiningDiacriticalMarks = '/[\x{0300}-\x{036f}]/u';

        public static function slug($text) {
            $unaccent = preg_replace(self::$combiningDiacriticalMarks, '', Normalizer::normalize($text, Normalizer::FORM_D));
            $onlyalpha = preg_replace('/[^\w]/', ' ', $unaccent);
            $dashed = preg_replace('/\s+/', '-', trim($onlyalpha));
            return strtolower($dashed);
        }
    }

}

?>