<?php

namespace App\Models {

    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;

    class Orders extends Model {
        
        use HasFactory;

        public static $createDefinition = [
            "user_id"=>"required|exists:users,id",
            "client"=>"required|max:128",
            "total"=>"required|numeric"
        ];

        public static $createMessages = [
            'user_id.required'=>'El ID de usuario es requerido',
            'user_id.exists'=>'El ID de usuario es invalido',
            'client.required'=>'El nombre del cliente es requerido',
            'client.max'=>'El nombre del cliente debe tener entre 1 y :max caractéres',
            "total.required"=>"El total es obligatorio",
            "total.numeric"=>"El total debe ser numérico"
        ];

        protected $fillable = [
            "user_id",
            "client",
            "total"
        ];

    }

}

?>