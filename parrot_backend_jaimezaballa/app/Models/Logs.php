<?php

namespace App\Models {

    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;

    class Logs extends Model {
        
        use HasFactory;

        public static $createDefinition = [
            "user_id"=>"required|exists:users,id",
            "controller"=>"required|max:255",
            "action"=>"required|max:128",
            "message"=>"required"
        ];

        public static $createMessages = [
            'user_id.required'=>'El ID de usuario es requerido',
            'user_id.exists'=>'El ID de usuario es invalido',
            'controller.required'=>'El nombre del controlador es requerido',
            'controller.max'=>'El nombre del controlador debe tener entre 1 y :max caractéres',
            'action.required'=>'La acción del controlador es requerida',
            'action.max'=>'La acción del controlador debe tener entre 1 y :max caractéres',
            'message.required'=>'El mensaje de error es requerido'
        ];

        protected $fillable = [
            "user_id",
            "controller",
            "action",
            "message"
        ];

    }
}

?>