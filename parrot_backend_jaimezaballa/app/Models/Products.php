<?php

namespace App\Models {

    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;

    class Products extends Model {

        use HasFactory;

        public static $createDefinition = [
            "name"=>"required|max:128|unique:products,name",
            "slug"=>"required|max:128|unique:products,name",
            "unitprice"=>"required|numeric",
            "quantity"=>"required|numeric"
        ];

        public static $createMessages = [
            'name.required'=>'El nombre de producto es obligatorio',
            'name.max'=>'El nombre de producto puede tener hasta :max caractéres',
            'name.unique'=>'Ese nombre de producto ya esta ocupado',
            "unitprice.required"=>"El precio unitario es obligatorio",
            "unitprice.numeric"=>"El precio unitario debe ser numérico",
            "quantity.required"=>"La cantidad es obligatoria",
            "quantity.numeric"=>"La cantidad debe ser numérica"
        ];

        protected $fillable = [
            "name",
            "slug",
            "description",
            "unitprice",
            "quantity"
        ];

    }

}

?>
