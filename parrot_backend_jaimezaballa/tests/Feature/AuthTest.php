<?php

namespace Tests\Feature {

    use Tests\TestCase;
    use Database\Factories\UserFactory;

    class AuthTest extends TestCase {

        protected static $userTest = null;
        protected static $token = null;

        private static function getUserTest() {
            if (is_null(self::$userTest)) {
                $userFactory = new UserFactory();
                $newUser = $userFactory->definition();
                $newUser['password_confirmation'] = $newUser['password'];
                self::$userTest = $newUser;
            }
            return self::$userTest;
        }
        
        public function testRegister() {
            $user = self::getUserTest();
            $response = $this->postJson('/api/register', $user);
            if($response->assertStatus(201)->assertJsonStructure(['success', 'user', 'token'])) {
                $data = $response->getData(true);
                self::$token = $data["token"];
            }
        }

        public function testAuthenticateWithTokenWorks() {
            $response = $this->withHeaders([
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.self::$token,
            ])->post('/api/me', ['name' => 'Sally']);
            $response->assertJsonStructure(["user"=>[
                "id",
                "name",
                "email",
                "email_verified_at",
                "created_at",
                "updated_at"
            ]]);
        }

        public function testRegisterDuplicateShouldNotPass() {
            $user = self::getUserTest();
            $response = $this->postJson('/api/register', $user);
            $response->assertStatus(400)->assertJson([
                "success"=>false,
                "messages"=>[
                    "email"=>"El correo ".$user["email"]." ya esta ocupado"
                ]
            ]);
        }

        public function testRegisterWithoutParametersShouldNotPass() {
            $response = $this->postJson('/api/register', []);
            $response->assertStatus(400)->assertJson([
                "success"=> false,
                "messages"=> [
                    "name"=> "El nombre de usuario es obligatorio",
                    "email"=> "El correo electrónico es obligatorio",
                    "password"=> "Debes especificar una contraseña"
                ]
            ]);
        }
    }

}

?>