<?php

namespace Tests\Feature {

    use App\Models\User;
    use Tests\TestCase;
    use Laravel\Sanctum\Sanctum;

    class OrdersTest extends TestCase {

        protected static $xsrfToken = null;

        //$cookies = $this->getCookies($response);
        private function getCookies($response) {
            $cookies = [];
            try {
                collect($response->headers->all())->map(function($header, $key) use (&$cookies) {
                    if ($key == 'set-cookie') {
                        foreach ($header as $item) {
                            if (stripos($item, "XSRF-TOKEN") !== false) {
                                $itemParts = explode(";", $item);
                                foreach ($itemParts as $itemPart) {
                                    $keyValue = explode("=", $itemPart);
                                    $key = strtolower(trim($keyValue[0]));
                                    $value = trim($keyValue[1]);
                                    $cookies[$key] = $value;
                                }
                            }
                        }
                    }
                });
            } catch (\Exception $e) {}
            return $cookies;
        }

        public function testGettingToken() {
            $response = $this->get('/sanctum/csrf-cookie');
            $response->assertStatus(204);
            $response->assertCookie("XSRF-TOKEN");
        }
        
        public function testAuthenticateWithTokenWorks() {
            $firstUser = User::first();
            Sanctum::actingAs($firstUser, ['*']);
            $response = $this->postJson('/api/me');
            $response->assertJsonStructure(["user"=>["id","name","email","email_verified_at","created_at","updated_at"]]);
        }

        public function testOrderCreateIdealWorks() {
            $firstUser = User::first();
            Sanctum::actingAs($firstUser, ['*']);
            $postData = [
                "client"=>"Steven Universe",
                "products"=>[
                    [
                        "name"=>"Banana Split",
                        "description"=>"Helado de sabores múltiples con relleno de bananas y cubierta sabor chocolate",
                        "unitprice"=>45,
                        "quantity"=>1
                    ], [
                        "name"=>"Enchiladas mineras",
                        "description"=>"Tortillas enrolladas rellenas de queso gratinado cubiertas con mezcla de chiles rojos y crema de rancho, acompañas con ensalada.",
                        "unitprice"=>35,
                        "quantity"=>1
                    ]
                ]
            ];
            $response = $this->postJson('/api/orders', $postData);
            $response->assertJsonStructure(["success", "order"=>["client","user_id","total","updated_at","created_at","id","products"]]);
        }

        public function testOrderCreateBadEstructure() {
            $firstUser = User::first();
            Sanctum::actingAs($firstUser, ['*']);
            $postData = [
                "client"=>"Fulanito Pérez",
                "products"=>[
                    [
                        "description"=>"Helado de sabores múltiples con relleno de bananas y cubierta sabor chocolate",
                        "unitprice"=>45,
                        "quantity"=>1
                    ], [
                        "description"=>"Tortillas enrolladas rellenas de queso gratinado cubiertas con mezcla de chiles rojos y crema de rancho, acompañas con ensalada.",
                        "unitprice"=>35,
                        "quantity"=>1
                    ]
                ]
            ];
            $response = $this->postJson('/api/orders', $postData);
            $response->assertStatus(400);
        }
    }
}

?>