<?php

namespace Database\Seeders {

    use App\Models\PersonalAccessTokens;
    use App\Models\User;
    use App\Models\UsuariosModel;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Str;

    class UsuariosSeeder extends Seeder {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run() {
            // Usuario inicial
            $userData = [
                'name' => 'Jaime Zaballa',
                'email' => 'muchologo@gmail.com',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10)
            ];
            $user = User::create($userData);
            // Token del usuario inicial => 1|dL1d0rO4LZ5qpfjLZydkgaOGQoTGcvDHmVNVbqnP
            $tokenData = [
                'tokenable_type'=>'App\Models\User',
                'tokenable_id'=>$user->id,
                'name'=>'myapptoken',
                'token'=>'a2aca477b761d0388e0257a533f97bd540aa3267698d585b502274eab7e2b739',
                'abilities'=>'["*"]',
                'last_used_at'=>date('Y-m-d H:i:s')
            ];
            PersonalAccessTokens::create($tokenData);
        }
    }

}
?>